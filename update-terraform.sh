WDIR="/tmp/terraform/"

echo "Checking terraform version"
CURRENTVERSION=`terraform --version |grep -i "terraform v" |awk '{print $2}' |cut -d 'v' -f 2`
NEWVERSION=`terraform --version |grep -i download |awk '{print $2}' |cut -d '.' -f 1,2,3`
#NEWVERSION="0.12.16"

if [[ "$CURRENTVERSION" != "$NEWVERSION" &&  ! -z "$NEWVERSION"  ]];
then
    mkdir $WDIR
    echo "New version "$NEWVERSION" available continue to update..."
    echo "Getting SUM checksums"
    wget -q https://releases.hashicorp.com/terraform/"$NEWVERSION"/terraform_"$NEWVERSION"_SHA256SUMS -O "$WDIR"terraform.SUM

    SUMWEB=`grep linux_amd64 /tmp/terraform/terraform.SUM |awk '{print $1}'`

    echo "Getting Terraform $NEWVERSION"
    wget -q https://releases.hashicorp.com/terraform/"$NEWVERSION"/terraform_"$NEWVERSION"_linux_amd64.zip -O "$WDIR"terraform.zip

    SUMFILE=`sha256sum "$WDIR"terraform.zip |awk '{print $1}'`
    if [ "$SUMWEB" == "$SUMFILE" ]
    then
        echo "SUM Matches... continue to update"
        unzip -o "$WDIR"terraform.zip -d "$WDIR"
	echo "Copying to /usr/bin folder..."
        sudo cp "$WDIR"terraform /usr/bin/terraform
    else
        echo "SUM DOES NOT MATCH"
    fi

    
    echo "Cleaning up temp folder"
    rm -rf $WDIR

else
    echo "Version $CURRENTVERSION up to date"
fi



